import DataController from "../common/DataController"

export class StartModel {
    constructor() { };
    public getSelectLevel(): number {
        return DataController.getNumber("selectLevelID", 0);
    }
}