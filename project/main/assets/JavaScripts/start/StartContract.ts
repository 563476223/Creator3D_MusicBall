export interface IStartUI{
    init():void;
    initData():void;
    initUI():void;
    initEvent():void;
    /**
     * 加载并播放当前选中歌曲
     */
    playSelectMusic():void;
    /**
     * 开始按钮点击事件
     */
    btnStartGameEvent():void;
    /**
     * 上一曲按钮点击事件
     */
    btnLastMusicEvent():void;
    /**
     * 下一曲按钮点击事件
     */
    btnNextMusicEvent():void;
}