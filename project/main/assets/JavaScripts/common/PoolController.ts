import { Prefab,Node, NodePool, instantiate } from "cc";

const { ccclass, property } = cc._decorator;
/**
 * zongyuan.yang   对象池管理类
 */
@ccclass
export class PoolController {
    private static pool_dict: any = {};
    public static initDictPool(name: string, num: number, prefab: Prefab | Node): void {
        if (!this.pool_dict.hasOwnProperty(name)) {
            let pool: NodePool = new NodePool();
            for(let i=0;i<num;i++){
                let item:Node=instantiate(prefab);
                pool.put(item);
            }
            this.pool_dict[name] = pool;
        }
    }
    public static getDictPool(name: string): Node {
        if (!this.pool_dict.hasOwnProperty(name)) {
            console.error(name+" 对象池不存在");
            return null;
        }
        let node: Node = null;
        let pool:NodePool=this.pool_dict[name];
        if(pool.size()>0){
            node=pool.get();
        }else{
            console.error(name+" 对象池暂无空闲可用对象");
        }
        return node;
    }
    public static recycleDictPool(name: string, node: Node): void {
        if (!this.pool_dict.hasOwnProperty(name)) {
            console.error(name+" 对象池不存在");
        }
        node.removeFromParent();
        this.pool_dict[name].put(node)
    
    }
}
