
const { ccclass, property } = cc._decorator;
/**
 * zongyuan.yang   本地数据管理
 */
@ccclass
export default class DataController {
    //获取数值
    public static getNumber(key: string, defaultValue = 0) {
        let value = this.getItem(key);
        if (value) {
            return parseFloat(value);
        }
        return defaultValue;
    }

    //增加数值
    public static addNumber(key: string, add: number, max?: number) {
        let old = this.getNumber(key);
        let value = old + add;
        if (max != null) {
            value = Math.max(Math.min(value, max), 0);
        }
        this.setNumber(key, value);
    }

    //设置数值
    public static setNumber(key: string, value: number) {
        this.setItem(key, value.toString());
    }

    //获取布尔值
    public static getBoolean(key: string, defaultValue = false) {
        let value = this.getItem(key);
        if (value == "1") {
            return true;
        } else if (value == "0") {
            return false;
        }
        return defaultValue;
    }

    //设置布尔值
    public static setBoolean(key: string, value: boolean) {
        if (value) {
            this.setItem(key, "1");
        } else {
            this.setItem(key, "0");
        }
    }

    //通过key获取字符串
    public static getString(key: string, defaultValue = "") {
        let value = this.getItem(key);
        return value ? value : defaultValue;
    }

    //通过key设置字符串
    public static setString(key: string, value: string) {
        this.setItem(key, value);
    }

    private static setItem(key: string, value: string) {
        cc.sys.localStorage.setItem(key, value)
    }

    private static getItem(key: string): string {
        return cc.sys.localStorage.getItem(key)
    }

    public static getObject(key, defaultValue) {
        let str = this.getString(key);
        if (str == null || str == "") {
            return defaultValue;
        }
        return JSON.parse(str);
    }

    public static setObject(key, object) {
        let str = JSON.stringify(object);
        this.setString(key, str);
    }




    public static setMaxScore(score: number = 0): void {
        if (score > this.getMaxScore()) {
            this.setNumber("max_score", score);
        }
    }
    public static getMaxScore(): number {
        return this.getNumber("max_score");
    }
    // public static setMaxScore2(score: number = 0): void {
    //     if (score > this.getMaxScore()) {
    //         this.setNumber("max_score_speed", score);
    //     }
    // }
    // public static getMaxScore2(): number {
    //     return this.getNumber("max_score_speed");
    // }

    public static setAuthorize(state:boolean) {
        this.setBoolean("Authorize", state);
    }
    public static isAuthorize() {
        return this.getBoolean("Authorize", false);
    }
    public static getNickname() {
        return this.getString("Nickname", "");
    }

    public static setNickname(name) {
        this.setString("Nickname", name);
    }
    public static getPhoto() {
        return this.getString("Photo", "");
    }

    public static setPhoto(value) {
        this.setString("Photo", value);
    }
    public static setOpenid(value) {
        this.setString("Openid", value);
    }
    public static getOpenid() {
        return this.getString("Openid", "");
    }

}
